Pyspark streaming

I have used Apache Kafka(https://kafka.apache.org/) for streaming tweets into pyspark.

There are three files in code.
1. twitter_stream.py
    It extracts tweets using twython library and publishes it to kafka

2. print_streamed_tweets.py
    It predicts the sentiment of tweet using nltk and prints it.

3. sentiment_analysis.py
    In this file I am trying to train and improve classifier.

