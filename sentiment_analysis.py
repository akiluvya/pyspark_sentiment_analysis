import string
import pdb

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from pyspark import SparkContext
from pyspark.mllib.feature import HashingTF
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import NaiveBayes
from pyspark.mllib.classification import SVMWithSGD

# Module-level global variables for the `tokenize` function below
PUNCTUATION = set(string.punctuation)
STOPWORDS = set(stopwords.words('english'))
STEMMER = PorterStemmer()

def tokenize(sentence):
    tokens = word_tokenize(sentence)
    lowercased = [t.lower() for t in tokens]
    no_punctuation = []
    for word in lowercased:
        punct_removed = ''.join([letter for letter in word if not letter in PUNCTUATION])
        no_punctuation.append(punct_removed)
    no_stopwords = [w for w in no_punctuation if not w in STOPWORDS]
    stemmed = [STEMMER.stem(w) for w in no_stopwords]
    return [w for w in stemmed if w]



sc = SparkContext()

#loading dataset
data_raw = sc.textFile('./sentiment_dataset.csv')


# Extract relevant fields in dataset -- category label and text content
data_pared = data_raw.map(lambda line: (line.split(",", 3)))

data_cleaned = data_pared.map(lambda (id_, label, source, text): (label, tokenize(text)))

# Hashing term frequency vectorizer with 50k features
htf = HashingTF(50000)

# Create an RDD of LabeledPoints using category labels as labels and tokenized, hashed text as feature vectors
data_hashed = data_cleaned.map(lambda (label, text): LabeledPoint(label, htf.transform(text)))

#persist the data
data_hashed.persist()

# Split data 70/30 into training and test data sets
train_hashed, test_hashed = data_hashed.randomSplit([0.7, 0.3])

#Train a Naive Bayes model
#model = NaiveBayes.train(train_hashed)
model = SVMWithSGD.train(train_hashed)

prediction_and_labels = test_hashed.map(lambda point: (model.predict(point.features), point.label))
correct = prediction_and_labels.filter(lambda (predicted, actual): predicted == actual)

accuracy = correct.count() / float(test_hashed.count())
print dir(model)
print "Accuracy : " + str(accuracy * 100)
